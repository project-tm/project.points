<?

namespace Project\Points\Event;

use cUser,
    Project\Points\Points,
    Project\Points\Model\PointsTable;

class Sale {

    public static function OnSaleOrderPaid($event) {
        $arFields = $event->getParameters();
        if ($arFields['ENTITY']) {
            $orderId = $arFields['ENTITY']->getBasket()->getOrderId();
            $isPaid = $arFields['ENTITY']->isPaid();
            if ($isPaid) {
                if (Points::isNew(array(
                            'USER_ID' => cUser::getId(),
                            'TYPE' => 'ORDER',
                            'ELEMENT_ID' => $orderId,
                        ))) {
                    Points::add(array(
                        'USER_ID' => cUser::getId(),
                        'MESSAGE' => 'за заказ №' . $orderId,
                        'TYPE' => 'ORDER',
                        'IBLOCK_ID' => 0,
                        'ELEMENT_ID' => $orderId,
                        'POINTS' => 10,
                        'NAME' => 'Заказ №' . $orderId,
                        'URL' => '/shop/personal/order/detail/' . $orderId . '/',
                    ));
                }
            } else {
                Points::Delete(array(
                    'USER_ID' => cUser::getId(),
                    'TYPE' => 'ORDER',
                    'IBLOCK_ID' => 0,
                    'ELEMENT_ID' => $orderId,
                    'POINTS' => 10,
                ));
            }
        }
    }

}
