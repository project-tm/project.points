<?

namespace Project\Points\Event;

use CDBResult,
    CIBlockElement,
    Igromafia\Game,
    Project\Points\Config,
    Project\Points\Points,
    Project\Points\Model\PointsTable;

class Iblock {

    private static function get($arFields) {
        $arFilter = array(
            'ID' => $arFields['ID'],
        );
        if (isset($arFields['IBLOCK_ID'])) {
            $arFilter['IBLOCK_ID'] = $arFields['IBLOCK_ID'];
        }
        $arSelect = array('ID', 'ACTIVE', 'NAME', 'CREATED_BY', 'DETAIL_PAGE_URL');
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        return $res->GetNext();
    }

    public static function OnBeforeIBlockElementDelete($ID) {
        $arItem = self::get(array(
                    'ID' => $ID
        ));
        if ($arItem and $arItem['ACTIVE'] == 'Y') {
            Points::Delete(array(
                'USER_ID' => $arItem['CREATED_BY'],
                'TYPE' => 'IBLOCK',
                'IBLOCK_ID' => $arItem['IBLOCK_ID'],
                'ELEMENT_ID' => $arItem['ID'],
                'POINTS' => 10,
            ));
        }
    }

    public static function OnAfterIBlockElementUpdate(&$arFields) {
        if (empty($arFields['RESULT'])) {
            return;
        }
        if (isset($arFields['ACTIVE']) and isset($arFields['IBLOCK_ID']) and in_array($arFields['IBLOCK_ID'], Config::IBLOCK_MAP)) {
            $arItem = self::get($arFields);
            if ($arFields['ACTIVE'] == 'Y') {
                if (Points::isNew(array(
                            'USER_ID' => $arItem['CREATED_BY'],
                            'TYPE' => 'IBLOCK',
                            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                            'ELEMENT_ID' => $arFields['ID'],
                        ))) {
                    Points::add(array(
                        'USER_ID' => $arItem['CREATED_BY'],
                        'MESSAGE' => 'за публикацию',
                        'TYPE' => 'IBLOCK',
                        'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                        'ELEMENT_ID' => $arFields['ID'],
                        'POINTS' => 10,
                        'NAME' => $arItem['NAME'],
                        'URL' => $arItem['DETAIL_PAGE_URL'],
                    ));
                }
            } else {
                Points::Delete(array(
                    'USER_ID' => $arItem['CREATED_BY'],
                    'TYPE' => 'IBLOCK',
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                    'ELEMENT_ID' => $arFields['ID'],
                    'POINTS' => 10,
                ));
            }
        }
    }

}
