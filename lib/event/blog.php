<?

namespace Project\Points\Event;

use cUser,
    CBlogComment,
    CBlogPost,
    Bitrix\Main\Loader,
    Project\Points\Points;

class Blog {

    public static function OnCommentAdd($ID, $arFields) {
        $arComment = CBlogComment::GetByID($ID);
        if (Loader::includeModule('blog') and Points::isNew(array(
                    'USER_ID' => $arComment['AUTHOR_ID'],
                    'TYPE' => 'BLOG',
                    'ELEMENT_ID' => $ID,
                ))) {
            $arPost = CBlogPost::GetByID($arFields['POST_ID']);
            Points::add(array(
                'USER_ID' => $arComment['AUTHOR_ID'],
                'MESSAGE' => 'комментарий на сайте',
                'TYPE' => 'BLOG',
                'IBLOCK_ID' => 0,
                'ELEMENT_ID' => $ID,
                'POINTS' => 1,
                'NAME' => $arPost['TITLE'],
                'URL' => str_replace('#comment_id#', $ID, $arFields['PATH']),
            ));
        }
    }

    public static function OnCommentDelete($ID) {
        $arComment = CBlogComment::GetByID($ID);
        Points::Delete(array(
            'USER_ID' => $arComment['AUTHOR_ID'],
            'TYPE' => 'BLOG',
            'ELEMENT_ID' => $ID,
            'POINTS' => 1,
        ));
    }

}
