<?php

namespace Project\Points\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class PointsTable extends DataManager {

    public static function getTableName() {
        return 'project_points';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('USER_ID'),
            new Main\Entity\StringField('TYPE'),
            new Main\Entity\StringField('IBLOCK_ID'),
            new Main\Entity\StringField('ELEMENT_ID'),
            new Main\Entity\StringField('MESSAGE'),
            new Main\Entity\IntegerField('POINTS'),
            new Main\Entity\IntegerField('NAME'),
            new Main\Entity\IntegerField('URL')
        );
    }

}
