<?php

namespace Project\Points;

use cUser,
    CDBResult,
    Bitrix\Main\Loader,
    Project\Points\Model\PointsTable,
    Project\Support\Message;

class Points {

    public static function isNew($arFilter) {
        return !self::get($arFilter);
    }

    private static function get($arFilter) {
        $rsData = PointsTable::getList(array(
                    "select" => array('ID'),
                    "filter" => $arFilter,
        ));
        $rsData = new CDBResult($rsData);
        return $rsData->Fetch();
    }

    public static function Delete($arFields) {
        if ($arItem = self::get(array(
                    'USER_ID' => $arFields['USER_ID'],
                    'TYPE' => $arFields['TYPE'],
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                    'ELEMENT_ID' => $arFields['ELEMENT_ID'],
                ))) {
            PointsTable::Delete($arItem['ID']);
            $arFields['POINTS'] = - $arFields['POINTS'];
            self::update($arFields);
        }
    }

    public static function update($arFields) {
        pre($arFields);
        $arUser = cUser::getById($arFields['USER_ID'])->Fetch();
        if ($arUser) {
            $user = new CUser;
            $fields = Array(
                "UF_BALL" => $arUser['UF_BALL'] + $arFields['POINTS'],
            );
            $user->Update($arFields['USER_ID'], $fields);
//            preExit($fields);

            $arData = array(
                'THEME' => 'Баллы',
                'MESSAGE' => 'Вам начисленно: ',
            );
            switch (abs($arFields['POINTS'])) {
                case 1:
                    $arData['MESSAGE'] .= $arFields['POINTS'] . ' балл';
                    break;

                case 2:
                case 3:
                case 4:
                    $arData['MESSAGE'] .= $arFields['POINTS'] . ' балла ';
                    break;

                default:
                    $arData['MESSAGE'] .= $arFields['POINTS'] . ' баллов';
                    break;
            }
            $arData['PARAM1'] = $arFields['POINTS'];
            $arData['MESSAGE'] .= ' ' . $arFields['MESSAGE'];
            $arData['MESSAGE'] .= PHP_EOL . PHP_EOL . '[URL=' . $arFields['URL'] . ']' . $arFields['NAME'] . '[/URL]';

            if ($arFields['POINTS'] > 0) {
                $arData['URL'] = Message::add('points', $arData);
//                preExit($arData);
            }
        }
    }

    public static function add($arFields) {
        PointsTable::add($arFields);
        self::update($arFields);
    }

}
