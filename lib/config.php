<?php

namespace Project\Points;

use Igromafia\Game;

class Config {

    const IBLOCK_MAP = array(
        Game\Config::ARTICLE_IBLOCK,
        Game\Config::NEWS_IBLOCK
    );

}
