<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <div class="bottom <?= $arParams['WRAPPER']["CONTANER"] ?>">
    <? } ?>
    <? if ($arParams['WRAPPER']['IS_FULL']) { ?>
        <div class="txt">НАЧИСЛЕННЫЕ БАЛЛЫ</div>
        <div class="filtr-tabs <?= $arParams['WRAPPER']["CONTANER_FILTER"] ?>">
            <? foreach ($arResult['TYPE'] as $key => $value) { ?>
                <a class="filter-element <? if (isset($value['SELECTED'])) { ?>active<? } ?>" data-filter="<?= $key ?>"><?= $value['NAME'] ?></a>
            <? } ?>
        </div>
        <div class="<?= $arParams['WRAPPER']["CONTANER_LIST"] ?>">
        <? } ?>
        <? foreach ($arResult['POINTS'] as $arItems) { ?>
            <div class="work-block">
                <div class="circle"><?= $arItems['POINTS'] ?></div>
                <div class="txt"><?= $arItems['MESSAGE'] ?></div>
                <a href="<?= $arItems['URL'] ?>" class="game"><?= $arItems['NAME'] ?></a>
                <div class="clear"></div>
            </div>
        <? } ?>
        <script>
<?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(<?= (int)$arResult['IS_NEXT'] ?>, 'class-hidden');
        </script>
        <? if ($arParams['WRAPPER']['IS_FULL']) { ?>
        </div>
        <div class="wrap_show-more">
            <div class="show-more mplavkadjo-show-more <?= $arParams['WRAPPER']["CONTANER_MORE"] ?>">Показать еще</div>
        </div>
    <? } ?>
    <? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    </div>
<? } ?>